import { BrowserModule } from '@angular/platform-browser';
import { DEFAULT_CURRENCY_CODE, LOCALE_ID, NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';
import { MenuComponent } from './menu/menu.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { AboutComponent } from './about/about.component';

registerLocaleData(localeDe);

@NgModule({
    declarations: [AppComponent, MenuComponent, AboutComponent],
    imports: [
        BrowserModule,
        HttpClientModule,
        RouterModule.forRoot([
            {
                path: '',
                pathMatch: 'full',
                redirectTo: 'menu',
            },
            {
                path: 'menu',
                loadChildren: () => import('@ginza-restaurant/food-menu').then(m => m.FoodMenuModule),
            },
            {
                path: 'about',
                component: AboutComponent,
            },
        ]),
        BrowserAnimationsModule,
        MatToolbarModule,
    ],
    providers: [
        {provide: LOCALE_ID, useValue: 'de'},
        {provide: DEFAULT_CURRENCY_CODE, useValue: 'EUR'},
    ],
    bootstrap: [AppComponent],
})
export class AppModule {
}

import { Component, Input, OnInit } from '@angular/core';

@Component({
    selector: 'ginza-menu',
    templateUrl: './menu.component.html',
    styleUrls: ['./menu.component.css'],
})
export class MenuComponent {

    @Input() title: string;

    menu: NavMenu[] = [
        {name: 'Speisekarte', link: '/menu'},
        {name: 'Impressum', link: '/about'},
    ];
}

export interface NavMenu {
    name: string;
    link: string;
}

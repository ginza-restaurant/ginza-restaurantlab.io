import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FoodMenuComponent } from './food-menu/food-menu.component';
import { RouterModule } from '@angular/router';
import { MatGridListModule } from '@angular/material/grid-list';

@NgModule({
    imports: [
        CommonModule,
        RouterModule.forChild([
            {path: '', component: FoodMenuComponent},
        ]),
        MatGridListModule,
    ],
    declarations: [FoodMenuComponent],
})
export class FoodMenuModule {
}

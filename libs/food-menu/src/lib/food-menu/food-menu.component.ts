import { Component, OnInit } from '@angular/core';
import { FoodMenuService, FoodMenu } from '../food-menu.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'ginza-food-menu',
    templateUrl: './food-menu.component.html',
    styleUrls: ['./food-menu.component.scss'],
})
export class FoodMenuComponent implements OnInit {

    foodMenu$: Observable<FoodMenu[]>;

    constructor(private foodMenuService: FoodMenuService) {
    }

    ngOnInit(): void {
        this.foodMenu$ = this.foodMenuService.getFoodMenu();
    }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
    providedIn: 'root',
})
export class FoodMenuService {

    constructor(private http: HttpClient) {
    }

    getFoodMenu(): Observable<FoodMenu[]> {
        return this.http.get<FoodMenu[]>(`/assets/data/food-menu.json`);
    }

}

export interface FoodMenu {
    group: string;
    items: FoodMenuItem[];
}

export interface FoodMenuItem {
    name: string;
    price: number;
}

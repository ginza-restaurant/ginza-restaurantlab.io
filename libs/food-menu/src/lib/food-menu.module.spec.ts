import { async, TestBed } from '@angular/core/testing';
import { FoodMenuModule } from './food-menu.module';

describe('FoodMenuModule', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [FoodMenuModule],
        }).compileComponents();
    }));

    it('should create', () => {
        expect(FoodMenuModule).toBeDefined();
    });
});

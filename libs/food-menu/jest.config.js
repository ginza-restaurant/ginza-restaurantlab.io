module.exports = {
    name: 'food-menu',
    preset: '../../jest.config.js',
    coverageDirectory: '../../coverage/libs/food-menu',
    snapshotSerializers: [
        'jest-preset-angular/build/AngularNoNgAttributesSnapshotSerializer.js',
        'jest-preset-angular/build/AngularSnapshotSerializer.js',
        'jest-preset-angular/build/HTMLCommentSerializer.js',
    ],
};
